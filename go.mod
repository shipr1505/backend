module gitlab.com/shipr/backend

go 1.14

require (
	github.com/NYTimes/gziphandler v1.1.1
	github.com/SevereCloud/vksdk v1.5.0
	github.com/alicebob/gopher-json v0.0.0-20180125190556-5a6b3ba71ee6 // indirect
	github.com/alicebob/miniredis v2.5.0+incompatible
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/golang/protobuf v1.3.5
	github.com/gomodule/redigo v2.0.0+incompatible // indirect
	github.com/gorilla/mux v1.7.3
	github.com/micro/cli/v2 v2.1.2
	github.com/micro/go-micro/v2 v2.4.0
	github.com/micro/go-plugins/registry/consul/v2 v2.3.0
	github.com/onsi/ginkgo v1.10.1 // indirect
	github.com/onsi/gomega v1.7.0 // indirect
	github.com/rs/cors v1.7.0
	github.com/segmentio/ksuid v1.0.2
	github.com/throttled/throttled v2.2.4+incompatible
	github.com/yuin/gopher-lua v0.0.0-20191220021717-ab39c6098bdb // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/yaml.v2 v2.2.4 // indirect
)
