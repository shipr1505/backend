package misc

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"github.com/dgrijalva/jwt-go"

	"github.com/micro/go-micro/v2/metadata"
)

// ClaimsContextKey is used for context
const ClaimsContextKey contextKey = "claims"

// TokenContextKey is used for extracting jwt token from context
const TokenContextKey contextKey = "token"

type contextKey string

var (
	// ErrTokenInvalid denotes a token was not able to be validated.
	ErrTokenInvalid = errors.New("JWT Token was invalid")
)

// ClaimsFromContext parses jwt claims from context
func ClaimsFromContext(ctx context.Context, jwtKey string) (*jwt.Token, error) {
	headers, ok := metadata.FromContext(ctx)
	if !ok {
		return nil, fmt.Errorf("couldn't extract metadata from context")
	}

	header, ok := headers["Auth"]
	if !ok {
		return nil, fmt.Errorf("authorization key not passed to context")
	}

	token, ok := extractTokenFromAuthHeader(header)
	if !ok {
		return nil, fmt.Errorf("couldn't extract token from header")
	}

	claims := &JWTClaims{}
	var keyFunc = func(tkn *jwt.Token) (interface{}, error) {
		return []byte(jwtKey), nil
	}
	t, err := jwt.ParseWithClaims(token, claims, keyFunc)
	if err != nil {
		return t, err
	}

	if !t.Valid {
		return t, ErrTokenInvalid
	}

	return t, err
}

func extractTokenFromAuthHeader(val string) (token string, ok bool) {
	authHeaderParts := strings.Split(val, " ")
	if len(authHeaderParts) != 2 || !strings.EqualFold(authHeaderParts[0], "bearer") {
		return "", false
	}

	return authHeaderParts[1], true
}
