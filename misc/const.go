package misc

const (
	RedisPairPrefix = "pair:"
)

// mongodb helper conviniece constants
const (
	MongoDBName  = "shippr"
	MongoPairCol = "pairs"
	MongoUserCol = "users"
)

// the number of matches one person can have
const (
	NumMatchesLimit = 15
)
