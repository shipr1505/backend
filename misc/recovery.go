package misc

import (
	"context"
	"errors"
	"fmt"

	"github.com/micro/go-micro/v2/server"
)

// RecoveryWrapper implements a recovery pattern for go micro server
func RecoveryWrapper(fn server.HandlerFunc) server.HandlerFunc {
	return func(ctx context.Context, req server.Request, rsp interface{}) (err error) {
		defer func() {
			r := recover()

			if r != nil {
				switch e := r.(type) {
				case string:
					err = errors.New(e)
				case error:
					err = e
				default:
					err = fmt.Errorf("panic: %v", r)
				}
			}

		}()

		err = fn(ctx, req, rsp)

		return
	}
}
