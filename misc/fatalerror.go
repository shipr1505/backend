package misc

import "fmt"

// FatalError is a wrapper for unexpected errors
// used when unpredicted errors occur
type FatalError struct {
	Err     error
	Context string
}

func (e FatalError) Error() string {
	return fmt.Sprintf("unexpected error: %v, context: %s", e.Err, e.Context)
}
