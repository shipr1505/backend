package misc

import (
	"context"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/shipr/backend/matchmaking/pkg/io"
	"net/http"
	"strings"

	"github.com/micro/go-micro/v2/metadata"
)

// HeaderToContext converts http bearer header to auth context
func HeaderToContext(h http.Header) context.Context {
	jwtToken := h.Get("Authorization")
	jwtToken = strings.ReplaceAll(jwtToken, "Bearer ", "bearer ")

	return metadata.NewContext(context.TODO(), map[string]string{
		"auth": jwtToken,
	})
}

// JWTClaims is user jwt claims. 'nuff said.
type JWTClaims struct {
	ID io.UID `json:"uid"`
	jwt.StandardClaims
}
