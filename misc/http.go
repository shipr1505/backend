package misc

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"reflect"
)

// WriteJSON writes json to http response writer
func WriteJSON(entity interface{}, w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(entity); err != nil {
		return err
	}

	return nil
}

// WriteError writes error to http response writer
func WriteError(err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)

	if err == nil {
		err = errors.New("internal server error")
	}

	json.NewEncoder(w).Encode(
		struct {
			Err string `json:"error"`
		}{
			err.Error(),
		},
	)
}

// ReadJSON reads json to struct
func ReadJSON(entity interface{}, r *http.Request) error {
	if err := json.NewDecoder(r.Body).Decode(entity); err != nil {
		return err
	}

	fmt.Println(reflect.TypeOf(entity))
	fmt.Println(entity)

	return nil
}
