FROM golang AS build

# go watcher doesn't work outside gopath
RUN mkdir -p /go/src/gitlab.com/shipr/backend
WORKDIR /go/src/gitlab.com/shipr/backend

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -mod vendor -ldflags "-w" -a -o /go/bin/gateway ./gateway

FROM scratch
COPY --from=build /go/bin/gateway ./
ENTRYPOINT ["/gateway"]