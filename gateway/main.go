package main

import (
	"fmt"
	"github.com/micro/cli/v2"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	mmclient "gitlab.com/shipr/backend/matchmaking/client"
	uclient "gitlab.com/shipr/backend/users/client"

	"github.com/micro/go-micro/v2"
	_ "github.com/micro/go-plugins/registry/consul/v2"
	"github.com/rs/cors"

	"github.com/NYTimes/gziphandler"

	"github.com/gorilla/mux"
	"github.com/throttled/throttled"
	"github.com/throttled/throttled/store/memstore"
)

var (
	httpAddr               string
	consulAddr             string
	rateLimitMaxRatePerMin int
	rateLimitMaxBurst      int
)

func main() {
	// register gateway service, connect to consul and rabbitmq
	service := micro.NewService(
		micro.Name("go.micro.srv.gateway"),

		micro.Flags(
			&cli.StringFlag{
				Name:  "http.addr",
				Value: ":8000",
				Usage: "Address for HTTP (JSON) server",
			},
			&cli.StringFlag{
				Name:  "consul.addr",
				Value: "consul:8500",
				Usage: "Consul agent address",
			},
			&cli.IntFlag{
				Name:  "ratelimit.maxpermin",
				Value: 50,
				Usage: "Max requests to single method per minute",
			},
			&cli.IntFlag{
				Name:  "ratelimit.maxburst",
				Value: 5,
				Usage: "Max burst for rate limiter",
			},
		),
	)

	service.Init(
		micro.Action(func(c *cli.Context) error {
			httpAddr = c.String("http.addr")
			consulAddr = c.String("consul.addr")
			rateLimitMaxRatePerMin = c.Int("ratelimit.maxpermin")
			rateLimitMaxBurst = c.Int("ratelimit.maxburst")
			return nil
		}),
	)

	os.Setenv("MICRO_REGISTRY_ADDRESS", consulAddr)
	os.Setenv("MICRO_REGISTRY", "consul")

	// Rate limiting domain.
	var httpRateLimiter throttled.HTTPRateLimiter
	{
		store, err := memstore.New(65536)
		if err != nil {
			panic(err)
		}

		quota := throttled.RateQuota{
			MaxRate:  throttled.PerMin(rateLimitMaxRatePerMin),
			MaxBurst: rateLimitMaxBurst,
		}

		rateLimiter, err := throttled.NewGCRARateLimiter(store, quota)
		if err != nil {
			panic(err)
		}

		httpRateLimiter = throttled.HTTPRateLimiter{
			RateLimiter: rateLimiter,
			VaryBy:      &throttled.VaryBy{Path: true},
		}
	}

	// Transport domain.
	r := mux.NewRouter()
	{
		handler := mmclient.New(service.Client())
		r.PathPrefix("/matchmaking").Handler(http.StripPrefix("/matchmaking", handler))
	}
	{
		handler := uclient.New(service.Client())
		r.PathPrefix("/users").Handler(http.StripPrefix("/users", handler))
	}

	// Interrupt handler.
	errc := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errc <- fmt.Errorf("%s", <-c)
	}()

	// HTTP transport.
	go func() {

		handler := cors.AllowAll().Handler(r)
		handler = httpRateLimiter.RateLimit(handler)
		handler = gziphandler.GzipHandler(handler)

		errc <- http.ListenAndServe(httpAddr, handler)
	}()

	panic(<-errc)
}
