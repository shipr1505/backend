package main

import (
	"log"
	"os"

	"github.com/micro/cli/v2"

	_ "github.com/micro/go-plugins/registry/consul/v2"

	"github.com/go-redis/redis"
	"github.com/micro/go-micro/v2"
	"gitlab.com/shipr/backend/matchmaking/pkg/pb"
	"gitlab.com/shipr/backend/matchmaking/pkg/svc"
	"gitlab.com/shipr/backend/misc"
	"gopkg.in/mgo.v2"
)

var (
	consulAddr string
	mongoAddr  string

	redisAddr string
	redisPswd string

	jwtSecret string
)

func main() {
	service := micro.NewService(
		micro.Name("go.micro.srv.matchmaking"),
		micro.WrapHandler(misc.RecoveryWrapper),

		micro.Address(":5000"),

		micro.Flags(
			&cli.StringFlag{
				Name:  "consul.addr",
				Value: "consul:8500",
				Usage: "Consul agent address",
			},
			&cli.StringFlag{
				Name:  "mongo.addr",
				Value: "mongo:27017",
				Usage: "MongoDB address",
			},
			&cli.StringFlag{
				Name:  "redis.addr",
				Value: "redis:6379",
				Usage: "Redis adress",
			},
			&cli.StringFlag{
				Name:  "redis.pswd",
				Usage: "Redis password",
			},
			&cli.StringFlag{
				Name:  "jwt.secret",
				Value: "ULPZAvI57zxLU5eGc18m",
				Usage: "JWT Secret",
			},
		),
	)
	service.Init(
		micro.Action(func(c *cli.Context) error {
			consulAddr = c.String("consul.addr")
			mongoAddr = c.String("mongo.addr")
			redisAddr = c.String("redis.addr")
			redisPswd = c.String("redis.pswd")
			jwtSecret = c.String("jwt.secret")
			return nil
		}),
	)

	os.Setenv("MICRO_REGISTRY", "consul")
	os.Setenv("MICRO_REGISTRY_ADDRESS", consulAddr)

	var handler *svc.MatchmakingHandler
	{
		msess, err := mgo.Dial(mongoAddr)
		if err != nil {
			log.Fatal(err)
		}

		client := redis.NewClient(
			&redis.Options{
				Addr:     redisAddr,
				Password: redisPswd,
				DB:       0, // use default DB
			},
		)
		_, err = client.Ping().Result()
		if err != nil {
			log.Fatal(err)
		}

		handler = svc.NewMatchmakingHandler(msess, client, jwtSecret)
	}

	pb.RegisterMatchMakingHandler(service.Server(), handler)

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
