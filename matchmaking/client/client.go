package client

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/micro/go-micro/v2/client"
	"gitlab.com/shipr/backend/matchmaking/pkg/pb"
	"gitlab.com/shipr/backend/misc"
)

// MatchMakingHTTPHandler implements matchmaking http gateway
type MatchMakingHTTPHandler struct {
	cl pb.MatchMakingService
}

// SamplePair wraps sample pair
func (h *MatchMakingHTTPHandler) SamplePair(w http.ResponseWriter, r *http.Request) {
	var request pb.SamplePairRequest
	if err := misc.ReadJSON(&request, r); err != nil {
		misc.WriteError(err, w)
		return
	}

	ctx := misc.HeaderToContext(r.Header)
	response, err := h.cl.SamplePair(ctx, &request)
	if err != nil {
		misc.WriteError(err, w)
		return
	}

	misc.WriteJSON(response, w)

}

// RatePair wraps rate pair
func (h *MatchMakingHTTPHandler) RatePair(w http.ResponseWriter, r *http.Request) {
	var request pb.RatePairRequest
	if err := misc.ReadJSON(&request, r); err != nil {
		misc.WriteError(err, w)
		return
	}

	ctx := misc.HeaderToContext(r.Header)
	response, err := h.cl.RatePair(ctx, &request)
	if err != nil {
		misc.WriteError(err, w)
		return
	}

	misc.WriteJSON(response, w)

}

// Matches wraps matches
func (h *MatchMakingHTTPHandler) Matches(w http.ResponseWriter, r *http.Request) {
	var request pb.MatchesRequest
	if err := misc.ReadJSON(&request, r); err != nil {
		misc.WriteError(err, w)
		return
	}

	ctx := misc.HeaderToContext(r.Header)
	response, err := h.cl.Matches(ctx, &request)
	if err != nil {
		misc.WriteError(err, w)
		return
	}

	misc.WriteJSON(response, w)
}

// New creates a new matchmaking client
func New(client client.Client) http.Handler {
	mmh := MatchMakingHTTPHandler{
		cl: pb.NewMatchMakingService("go.micro.srv.matchmaking", client),
	}

	r := mux.NewRouter()

	r.HandleFunc("/sample_pair", mmh.SamplePair)
	r.HandleFunc("/rate_pair", mmh.RatePair)
	r.HandleFunc("/matches", mmh.Matches)

	return r
}
