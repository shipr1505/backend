package io

import (
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/duration"
	"github.com/golang/protobuf/ptypes/timestamp"
	"gitlab.com/shipr/backend/matchmaking/pkg/pb"
	"time"
)

func MakePbPair(uid1 UID, uid2 UID, pairId string) (*pb.Pair, error) {
	return &pb.Pair{
		Uids:   []int64{int64(uid1), int64(uid2)},
		PairId: pairId,
	}, nil
}

func MakePbUser(user *User) (*pb.User, error) {
	pbBirthDate, err := ptypes.TimestampProto(user.BirthDate)
	if err != nil {
		return nil, err
	}
	return &pb.User{
		Id:        int64(user.ID),
		Name:      user.Name,
		Surname:   user.Surname,
		Gender:    user.Gender,
		BirthDate: pbBirthDate,
		PhotoLink: user.PhotoLink,
	}, nil
}

func MakeTime(ts *timestamp.Timestamp) time.Time {
	if ts == nil {
		return time.Unix(0, 0)
	}
	return time.Unix(ts.Seconds, int64(ts.Nanos))
}

func MakeDuration(d *duration.Duration) time.Duration {
	if d == nil {
		return time.Duration(0)
	}
	return time.Duration(d.Seconds) * (time.Second / time.Nanosecond) + time.Duration(d.Nanos)
}
