package io

import (
	"testing"
	"time"
)

func TestAgeInterval(t *testing.T) {
	i := AgeInterval{
		MinAge: 17,
		MaxAge: 20,
	}

	tests := map[time.Time]bool{
		time.Now().Add(-Year * 16): false,
		time.Now().Add(-Year * 18): true,
		time.Now().Add(-Year * 20): false,
	}

	for age, r := range tests {
		t.Run(
			age.String(),
			func(t *testing.T) {
				if i.In(age) != r {
					t.Errorf("%t ne %t", r, i.In(age))
				}
			})
	}
}

func TestAdj(t *testing.T) {
	m := *adjMatrix(
		[]User{
			{ID: "A", Friends: []ID{"D"}},
			{ID: "B", Friends: []ID{"C", "D"}},
			{ID: "C", Friends: []ID{"B"}},
			{ID: "D", Friends: []ID{"A", "B"}},
		},
	)

	t.Log(m)
}
