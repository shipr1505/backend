package io

import (
	"time"
)

const Year = time.Hour * 24 * 365

// User stores user data
// User is passed to methods with a pointer to avoid copying (Friends can be big)
type User struct {
	// UID matches vk id
	ID UID `bson:"vk_id"`

	Name    string `bson:"name"`
	Surname string `bson:"surname"`

	// gender and birth date are user for mathchmaking
	Gender    bool      `bson:"gender"`
	BirthDate time.Time `bson:"birth_date"`

	PreferredGender bool        `bson:"preferred_gender"`
	PreferredAge    AgeInterval `bson:"preferred_age"`

	IsRegistered bool `bson:"is_registered"`

	PhotoLink string `bson:"photo_link"`

	FriendsUIDs []UID `bson:"friends"`
}

func (u *User) SetDefaults() {
	// TODO: Add more accurate defaults
	u.PreferredGender = !u.Gender
	u.PreferredAge = AgeInterval{MinAge: 0, MaxAge: Year * 100}
}

// Match tells if users are compatable
func (u *User) Match(user *User) bool {
	return u.PreferredAge.In(user.BirthDate) &&
		user.PreferredAge.In(u.BirthDate) &&
		u.Gender == user.PreferredGender &&
		user.Gender == u.PreferredGender &&
		user.ID != u.ID
}

// UID alias for vk id
type UID int

// AgeInterval is used for matching users by their age preferences
type AgeInterval struct {
	MinAge time.Duration `bson:"min_age"`
	MaxAge time.Duration `bson:"max_age"`
}

// In tells if given birthdate mathes the age gap
func (i AgeInterval) In(birthDate time.Time) bool {
	now := time.Now()
	younger := now.Add(-i.MinAge)
	older := now.Add(-i.MaxAge)

	return birthDate.After(older) && birthDate.Before(younger)
}

// Pair is used to store pair ratings
type Pair struct {
	Users  [2]UID `bson:"users"`
	S1  int64 `bson:"scores1"`
	S2  int64 `bson:"scores2"`
	S3  int64 `bson:"scores3"`
	S4  int64 `bson:"scores4"`
	S5  int64 `bson:"scores5"`
	S6  int64 `bson:"scores6"`
	S7  int64 `bson:"scores7"`
	S8  int64 `bson:"scores8"`
	S9  int64 `bson:"scores9"`
	S10 int64 `bson:"scores10"`
}
