protoc --proto_path=$GOPATH/src:. --micro_out=. --go_out=. matchmaking.proto

if [ -e matchmaking.pb.micro.go ]
then
  mv matchmaking.pb.micro.go matchmaking.micro.go
fi

ls *.pb.go | xargs -n1 -IX bash -c 'sed s/,omitempty// X > X.tmp && mv X{.tmp,}'
