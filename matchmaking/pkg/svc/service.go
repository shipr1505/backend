/*
Package svc implements shipper's matchmaking business logic
*/
package svc

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/shipr/backend/misc"
	"time"

	"github.com/golang/protobuf/proto"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/go-redis/redis"

	"github.com/segmentio/ksuid"

	"gitlab.com/shipr/backend/matchmaking/pkg/io"
	"gitlab.com/shipr/backend/matchmaking/pkg/pb"
)

const (
	pairTTL = time.Hour
)

// i is a convinience type for mongo querries
// in the end, go is strictly typed
type i []interface{}

// MatchmakingHandler implements pb.MatchmakingService
// samples pairs, computes matches, gathers statistics
type MatchmakingHandler struct {
	rdb *redis.Client
	mdb *mgo.Session

	mmaker Matchmaker

	jwtKey string
}

// NewMatchmakingHandler creates a new matchmaking handler instance
func NewMatchmakingHandler(mdb *mgo.Session, rdb *redis.Client, jwtKey string) *MatchmakingHandler {
	return &MatchmakingHandler{
		mdb:    mdb,
		rdb:    rdb,
		mmaker: &naiveMatchmaker{},
		jwtKey: jwtKey,
	}
}

// SamplePair samples a pair for a given user and stashes it in Redis later to be rated
func (h *MatchmakingHandler) SamplePair(ctx context.Context, request *pb.SamplePairRequest, reply *pb.SamplePairReply) error {
	token, err := misc.ClaimsFromContext(ctx, h.jwtKey)
	if err != nil {
		return err
	}
	claims := token.Claims.(*misc.JWTClaims)
	userID := claims.ID

	sess := h.mdb.Clone()
	defer sess.Close()
	c := sess.DB(misc.MongoDBName).C(misc.MongoUserCol)

	var userWithFriends struct {
		io.User
		Friends []io.User `bson:"user_friends"`
	}
	err = c.Pipe([]bson.M{
		{"$match": bson.M{"vk_id": userID}},
		{"$lookup": bson.M{
			"from":         misc.MongoUserCol,
			"localField":   "friends",
			"foreignField": "vk_id",
			"as":           "user_friends",
		}},
	}).One(&userWithFriends)
	if errors.Is(err, mgo.ErrNotFound) {
		return fmt.Errorf("no user in database with ID %s", userID)
	}
	if err != nil {
		return misc.FatalError{err, "getting friends aggregation pipeline"}
	}

	pair := h.mmaker.Sample(userWithFriends.Friends)
	pairID := ksuid.New().String()

	pbPair, err := io.MakePbPair(pair[0].ID, pair[1].ID, pairID)
	if err != nil {
		return misc.FatalError{err, "converting to protobuf error"}
	}

	data, err := proto.Marshal(pbPair)
	if err != nil {
		return misc.FatalError{err, "marshalling error"}
	}

	err = h.rdb.Set(misc.RedisPairPrefix+pairID, data, pairTTL).Err()
	if err != nil {
		return misc.FatalError{err, "redis set"}
	}

	pbUser1, err := io.MakePbUser(&pair[0])
	if err != nil {
		return misc.FatalError{err, "converting to protobuf error"}
	}

	pbUser2, err := io.MakePbUser(&pair[1])
	if err != nil {
		return misc.FatalError{err, "converting to protobuf error"}
	}

	reply.PairId = pairID
	reply.Users = []*pb.User{pbUser1, pbUser2}

	return nil
}

// RatePair rates a pair if the pair's id was stored in Redis
// redis caching is a safety feature for avoiding pair score cheating
func (h *MatchmakingHandler) RatePair(ctx context.Context, request *pb.RatePairRequest, reply *pb.RatePairReply) error {
	_, err := misc.ClaimsFromContext(ctx, h.jwtKey)
	if err != nil {
		return err
	}

	if request.Rating > 10 || request.Rating < 1 {
		return fmt.Errorf("rating must be between 1 and 10")
	}

	val, err := h.rdb.Get(misc.RedisPairPrefix + request.PairId).Result()
	if err == redis.Nil {
		return fmt.Errorf("pair id %s doesn't exist or has already been rated", request.PairId)
	} else if err != nil {
		return misc.FatalError{err, "redis get"}
	}
	defer h.rdb.Del(misc.RedisPairPrefix + request.PairId)

	var p pb.Pair
	if err := proto.Unmarshal([]byte(val), &p); err != nil {
		return misc.FatalError{err, "proto pair unmarshalling"}
	}

	// Here you can paste ids for testing purposes
	// p.Uids[0] = 122857489
	// p.Uids[1] = 137654241

	// increment rating in mongodb
	sess := h.mdb.Clone()
	defer sess.Close()
	c := sess.DB(misc.MongoDBName).C(misc.MongoPairCol)

	// update scores and fetch new document in one transaction
	var pair io.Pair
	if _, err := c.Find(
		bson.M{
			"users": bson.M{
				"$all": []bson.M{
					{"$elemMatch": bson.M{"$eq": p.Uids[0]}},
					{"$elemMatch": bson.M{"$eq": p.Uids[1]}},
				},
			},
		},
	).Apply(
		mgo.Change{
			Update: bson.M{
				"$set": bson.M{"users": []int64{p.Uids[0], p.Uids[1]}},
				"$inc": bson.M{
					"scores" + fmt.Sprint(request.Rating): 1,
				},
			},
			Upsert:    true,
			ReturnNew: true,
		},
		&pair,
	); err != nil {
		return misc.FatalError{err, "score upsert"}
	}

	fmt.Println(pair.Users[0], pair.Users[1])
	reply.Ratings = []int64{
		pair.S1,
		pair.S2,
		pair.S3,
		pair.S4,
		pair.S5,
		pair.S6,
		pair.S7,
		pair.S8,
		pair.S9,
		pair.S10,
	}

	return nil
}

// Matches returns the user's best matches
func (h *MatchmakingHandler) Matches(ctx context.Context, request *pb.MatchesRequest, reply *pb.MatchesReply) error {
	token, err := misc.ClaimsFromContext(ctx, h.jwtKey)
	if err != nil {
		return err
	}
	claims := token.Claims.(*misc.JWTClaims)

	sess := h.mdb.Clone()
	defer sess.Close()
	c := sess.DB(misc.MongoDBName).C(misc.MongoPairCol)

	var matches []struct {
		Users []io.User `bson:"users"`
		Score float64   `bson:"score"`
	}
	if err := assembleMongoPairScorePipeline(claims.ID, misc.NumMatchesLimit, c).All(&matches); err != nil {
		return misc.FatalError{err, "pair scoring aggregation pipeline"}
	}

	if len(matches) < 1 {
		return nil
	}

	reply.Matches = append(
		reply.Matches,
		&pb.MatchesReply_Match{
			User: &pb.User{
				Id:        174050430,
				Name:      "Denis",
				Surname:   "Mazur",
				Gender:    true,
				BirthDate: nil,
				PhotoLink: "https://pp.userapi.com/c844618/v844618789/127fc1/Sn5G4gSMCm8.jpg",
			},
			Score: 1.,
		},
	)

	for _, m := range matches {
		matchUser := m.Users[0]
		if matchUser.ID == claims.ID {
			matchUser = m.Users[1]
		}
		pbMatchUser, err := io.MakePbUser(&matchUser)
		if err != nil {
			continue
		}
		reply.Matches = append(
			reply.Matches,
			&pb.MatchesReply_Match{
				User:  pbMatchUser,
				Score: m.Score,
			},
		)
	}

	return nil
}

// Matching score is computed by sum($scores.1 * 1, $scores.2 * 2 ... $scores.3 * 3) / totals_scores * 10
func assembleMongoPairScorePipeline(uid io.UID, limit int, c *mgo.Collection) *mgo.Pipe {
	return c.Pipe(
		[]bson.M{
			{"$match": bson.M{"users": uid}},
			{"$lookup": bson.M{
				"from":         misc.MongoUserCol,
				"localField":   "users",
				"foreignField": "vk_id",
				"as":           "user_objects",
			}},
			{"$project": bson.M{
				"users": "$user_objects",
				"score": bson.M{
					"$divide": i{
						bson.M{
							"$sum": []bson.M{
								{"$multiply": i{"$scores1", 1}},
								{"$multiply": i{"$scores2", 2}},
								{"$multiply": i{"$scores3", 3}},
								{"$multiply": i{"$scores4", 4}},
								{"$multiply": i{"$scores5", 5}},
								{"$multiply": i{"$scores6", 6}},
								{"$multiply": i{"$scores7", 7}},
								{"$multiply": i{"$scores8", 8}},
								{"$multiply": i{"$scores9", 9}},
								{"$multiply": i{"$scores10", 10}},
							},
						},
						bson.M{
							"$multiply": i{
								bson.M{
									"$sum": []string{
										"$scores1",
										"$scores2",
										"$scores3",
										"$scores4",
										"$scores5",
										"$scores6",
										"$scores7",
										"$scores8",
										"$scores9",
										"$scores10"},
								},
								10,
							},
						},
					},
				},
			},
			},
			bson.M{"$sort": bson.M{"score": -1}},
			bson.M{"$limit": limit},
		},
	)
}
