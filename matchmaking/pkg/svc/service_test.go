package svc

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/proto"
	"github.com/segmentio/ksuid"
	"gitlab.com/shipr/backend/matchmaking/pkg/io"
	"gitlab.com/shipr/backend/matchmaking/pkg/pb"
	"gitlab.com/shipr/backend/misc"
	"gopkg.in/mgo.v2"
	"testing"
	"time"

	"github.com/alicebob/miniredis"
)

func TestMatchmakingHandler_SamplePair(t *testing.T) {
	// Warning - code that is under this function needs to be replaced whenever MatchMakingHander or SamplePair changes
	s, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
	defer s.Close()

	nmm := naiveMatchmaker{}
	mmh := matchmakingHandlerMock{
		rdb:    s,
		mdb:    nil,
		mmaker: &nmm,
	}

	reply := &pb.SamplePairReply{}
	mmh.SamplePair(context.Background(), nil, reply)

	fmt.Println(reply.Users[0].Name, reply.Users[1].Name)
	fmt.Println(reply.PairId)
	//fmt.Println(mmh.rdb.Get(RedisPairPrefix + reply.PairId))
	//p, err := mmh.rdb.Get(RedisPairPrefix + reply.PairId)
}

type matchmakingHandlerMock struct {
	rdb *miniredis.Miniredis
	mdb *mgo.Session

	mmaker Matchmaker
}

// SamplePair samples a pair for a given user and stashes it in Redis later to be rated
func (h *matchmakingHandlerMock) SamplePair(ctx context.Context, request *pb.SamplePairRequest, reply *pb.SamplePairReply) error {

	// TODO receive users
	//users, err := (io.User{}).GetFriends()
	//if err != nil {
	//return fmt.Errorf("couldn't get user's friends")
	//}

	users := []io.User{
		{
			ID:              "petyadruzh",
			Name:            "Петя",
			Surname:         "Дружинин",
			Gender:          true,
			BirthDate:       time.Now().Add(-io.Year * 16),
			PreferredAge:    io.AgeInterval{5, 80},
			PreferredGender: false,
		},
		{
			ID:              "vaspodruzh",
			Name:            "Василиса",
			Surname:         "Подружкина",
			Gender:          false,
			BirthDate:       time.Now().Add(-io.Year * 17),
			PreferredAge:    io.AgeInterval{10, 100},
			PreferredGender: true,
		},
	}

	pair := h.mmaker.Sample(users)
	pairId := ksuid.New().String()

	pbPair, err := io.MakePbPair(pair[0].ID, pair[1].ID, pairId)
	if err != nil {
		return misc.FatalError{err, "converting to protobuf error"}
	}

	data, err := proto.Marshal(pbPair)
	if err != nil {
		return misc.FatalError{err, "marshalling error"}
	}

	h.rdb.Set(misc.RedisPairPrefix+pairId, string(data))

	pbUser1, err := io.MakePbUser(pair[0])
	if err != nil {
		return misc.FatalError{err, "converting to protobuf error"}
	}

	pbUser2, err := io.MakePbUser(pair[1])
	if err != nil {
		return misc.FatalError{err, "converting to protobuf error"}
	}

	reply.PairId = pairId
	reply.Users = []*pb.User{pbUser1, pbUser2}

	return nil
}
