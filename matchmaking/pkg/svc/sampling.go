package svc

import (
	"math/rand"

	"gitlab.com/shipr/backend/matchmaking/pkg/io"
)

// Matchmaker samples pairs
type Matchmaker interface {
	Sample([]io.User) [2]io.User
}

// naiveMatchmaker samples random age and sex compatable pairs
type naiveMatchmaker struct {
}

// len(users) >= 2
func (nm *naiveMatchmaker) Sample(users []io.User) [2]io.User {
	if len(users) < 2 {
		panic("len(users) must be >= 2")
	}

	u := users[rand.Intn(len(users))]

	// select compatable candidates
	var candidates []io.User
	for _, c := range users {
		if u.Match(&c) {
			candidates = append(candidates, c)
		}
	}

	// sample random person if compatable not found
	if len(candidates) < 1 {
		u2 := users[rand.Intn(len(users))]
		for u2.ID == u.ID {
			u2 = users[rand.Intn(len(users))]
		}
		return [2]io.User{u, u2}
	}

	u2 := candidates[rand.Intn(len(candidates))]

	return [2]io.User{u, u2}
}
