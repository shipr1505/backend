protoc --proto_path=$GOPATH/src:. --micro_out=. --go_out=. users.proto

if [ -e users.pb.micro.go ]
then
  mv users.pb.micro.go users.micro.go
fi

ls *.pb.go | xargs -n1 -IX bash -c 'sed s/,omitempty// X > X.tmp && mv X{.tmp,}'