package svc

import (
	"context"
	"fmt"
	"github.com/SevereCloud/vksdk/api"
	mio "gitlab.com/shipr/backend/matchmaking/pkg/io"
	"gitlab.com/shipr/backend/misc"
	"gitlab.com/shipr/backend/users/pkg/io"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"net/url"
	"strconv"
	"time"

	"gitlab.com/shipr/backend/users/pkg/pb"
)

// UsersHandler implements users business logic
type UsersHandler struct {
	vkSecret       string
	vkServiceToken string

	mdb *mgo.Session
}

// NewUsersHandler creates a new users handler instance
func NewUsersHandler(vkSecret, vkServiceToken string, mdb *mgo.Session) *UsersHandler {
	return &UsersHandler{
		vkSecret:       vkSecret,
		vkServiceToken: vkServiceToken,
		mdb:            mdb,
	}
}

// Auth authenticates users using the vk urls
func (h *UsersHandler) Auth(ctx context.Context, request *pb.AuthRequest, reply *pb.AuthReply) error {
	if ok, _ := isValid(request.Url, h.vkSecret); !ok {
		return fmt.Errorf("bad credentials")
	}

	u, _ := url.Parse(request.Url)
	q, _ := url.ParseQuery(u.RawQuery)
	userID, _ := strconv.Atoi(q["vk_user_id"][0])

	token, err := generateToken([]byte(h.vkSecret), mio.UID(userID))
	if err != nil {
		return misc.FatalError{err, "failed go generate jwt token"}
	}

	reply.AccessToken = token

	vk := api.NewVK(h.vkServiceToken)
	// retrieve user data from vk api
	// if user's vk account is closed tell him that and exit
	ugResponse, err := vk.UsersGet(
		api.Params{
			"user_id": userID,
			"fields":  "sex,photo_200_orig",
		},
	)
	if err != nil {
		return misc.FatalError{err, "failed to call vk.users.get"}
	}

	// open data base connections and create bulk
	sess := h.mdb.Clone()
	defer sess.Close()
	c := sess.DB(misc.MongoDBName).C(misc.MongoUserCol)
	bulk := c.Bulk()
	bulk.Unordered()

	// if user's profile is public add friends upsert to bulk
	var friendsIDs []int
	if ugResponse[0].IsClosed {
		reply.PrivateAccount = true
	} else {
		gfResp, err := vk.FriendsGetFields(
			api.Params{
				"user_id": userID,
				"fields":  "sex,photo_200_orig",
			},
		)
		if err != nil {
			return misc.FatalError{err, "failed to call vk.friends.get"}
		}

		var friendUpserts []interface{}
		for _, friend := range gfResp.Items {
			friendID := friend.ID
			friendsIDs = append(friendsIDs, friendID)

			friendUpserts = append(
				friendUpserts,
				[]interface{}{
					bson.M{"vk_id": friendID},
					bson.M{
						"$set": bson.M{
							"photo_link": friend.Photo200Orig,
							"name":       friend.FirstName,
							"surname":    friend.LastName,
						},
						"$setOnInsert": bson.M{
							"vk_id":            mio.UID(friendID),
							"preferred_age":    mio.AgeInterval{0, 100 * mio.Year},
							"gender":           vkSexToBool(friend.Sex),
							"birth_date":       time.Now().Add(-20 * mio.Year), // TODO: handle bdate
							"preferred_gender": !vkSexToBool(friend.Sex),
						},
					},
				}...,
			)

		}
		bulk.Upsert(friendUpserts...)
	}

	// build user upsert and add to bulk
	bulk.Upsert(
		bson.M{
			"vk_id": userID,
		},
		bson.M{
			"$set": bson.M{
				"is_registered": true,
				"name":          ugResponse[0].FirstName,
				"surname":       ugResponse[0].LastName,
				"photo_link":    ugResponse[0].Photo200Orig,
				"friends":       friendsIDs,
			},
			"$setOnInsert": bson.M{
				"gender":           vkSexToBool(ugResponse[0].Sex),
				"birth_date":       time.Now().Add(-20 * mio.Year), // TODO: handle bdate
				"preferred_gender": !vkSexToBool(ugResponse[0].Sex),
				"preferred_age":    mio.AgeInterval{0, 100 * mio.Year},
			},
		},
	)

	_, err = bulk.Run()
	return err
}

// Refresh refreshes jwt tokens                                                         \/ (sic.)
func (h *UsersHandler) Refresh(ctx context.Context, request *pb.RefreshRequest, reply *pb.AuthReply) error {
	return fmt.Errorf("not implemented")
}

// UpdateProfile updates some of user's fields
func (h *UsersHandler) UpdateProfile(ctx context.Context, request *pb.UpdateProfileRequest, reply *pb.UpdateProfileReply) error {
	token, err := misc.ClaimsFromContext(ctx, h.vkSecret)
	if err != nil {
		return err
	}
	claims := token.Claims.(*misc.JWTClaims)
	userID := claims.ID

	sess := h.mdb.Clone()
	defer sess.Close()
	c := sess.DB(misc.MongoDBName).C(misc.MongoUserCol)

	set := bson.M{
		"preferred_gender": request.Profile.GetPreferredGender(),
		"birth_date":       mio.MakeTime(request.Profile.BirthDate),
		"preferred_age": mio.AgeInterval{
			MinAge: mio.MakeDuration(request.Profile.PreferredAge.GetMinAge()),
			MaxAge: mio.MakeDuration(request.Profile.PreferredAge.GetMaxAge()),
		},
	}

	if err := c.Update(
		bson.M{
			"vk_id": userID,
		},
		bson.M{
			"$set": set,
		},
	); err != nil {
		return misc.FatalError{err, "update profile"}
	}
	return nil
}

// UpdateProfile updates some of user's fields
func (h *UsersHandler) GetProfile(ctx context.Context, request *pb.GetProfileRequest, reply *pb.GetProfileReply) error {
	token, err := misc.ClaimsFromContext(ctx, h.vkSecret)
	if err != nil {
		return err
	}
	claims := token.Claims.(*misc.JWTClaims)
	userID := claims.ID

	sess := h.mdb.Clone()
	defer sess.Close()
	c := sess.DB(misc.MongoDBName).C(misc.MongoUserCol)

	var user mio.User
	if err := c.Find(
		bson.M{
			"vk_id": userID,
		},
	).One(&user); err != nil {
		return misc.FatalError{err, "get profile"}
	}

	pbProfile, err := io.MakePbProfile(&user)
	if err != nil {
		return misc.FatalError{err, "converting to protobuf error"}
	}
	reply.Profile = pbProfile
	return nil
}

// UpdateFriends UpdateFriends method receives list of user's friends and
// updates info about user's friends and creates friends as users if they don't already exist in a bulk.
func (h *UsersHandler) UpdateFriends(ctx context.Context, request *pb.UpdateFriendsRequest, reply *pb.UpdateFriendsReply) error {
	 token, err := misc.ClaimsFromContext(ctx, h.vkSecret)
	if err != nil {
		return err
	}
	claims := token.Claims.(*misc.JWTClaims)

	sess := h.mdb.Clone()
	defer sess.Close()
	c := sess.DB(misc.MongoDBName).C(misc.MongoUserCol)
	userID := claims.ID

	friends := request.GetFriends()

	var friendUpserts []interface{}
	var friendsIDs []int64
	for _, friend := range friends {
		friendsIDs = append(friendsIDs, friend.Id)

		friendUpserts = append(
			friendUpserts,
			[]interface{}{
				bson.M{"vk_id": friend.Id},
				bson.M{
					"$set": bson.M{
						"photo_link": friend.PhotoUrl,
						"name":       friend.Name,
						"surname":    friend.Surnmae,
					},
					"$setOnInsert": bson.M{
						"vk_id":            mio.UID(friend.Id),
						"preferred_age":    mio.AgeInterval{0, 100 * mio.Year},
						"gender":           friend.Gender,
						"birth_date":       time.Now().Add(-20 * mio.Year), // TODO: handle bdate
						"preferred_gender": !friend.Gender,
					},
				},
			}...,
		)
	}

	bulk := c.Bulk()
	bulk.Unordered()
	bulk.Upsert(friendUpserts...)
	bulk.Upsert(
		bson.M{
			"vk_id": userID,
		},
		bson.M{
			"$set": bson.M{"friends": friendsIDs},
		},
	)

	_, err = bulk.Run()
	return err
}

func vkSexToBool(sex int) bool {
	if sex == 0 {
		return false
	} else if sex == 1 {
		return false
	}
	return true
}
