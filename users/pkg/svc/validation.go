package svc

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"net/url"
	"strings"
)

func isValid(link, clientSecret string) (bool, error) {
	// Парсинг URL и query-параметров
	u, err := url.Parse(link)
	if err != nil {
		return false, err
	}
	rawParams, err := url.ParseQuery(u.RawQuery)
	if err != nil {
		return false, err
	}
	if len(rawParams["sign"]) == 0 {
		return false, fmt.Errorf("not found sign")
	}
	// Фильтруем vk_ параметры
	vkPrefix := make(url.Values)
	for key, values := range rawParams {
		if strings.HasPrefix(key, "vk_") {
			for _, value := range values {
				vkPrefix.Add(key, value)
			}
		}
	}
	// Генерируем хеш код
	mac := hmac.New(sha256.New, []byte(clientSecret))
	_, _ = mac.Write([]byte(vkPrefix.Encode())) // Encode сортирует по ключу
	expectedMAC := mac.Sum(nil)
	// Генерируем base64
	base64Sign := base64.StdEncoding.EncodeToString(expectedMAC)
	base64Sign = strings.ReplaceAll(base64Sign, "+", "-")
	base64Sign = strings.ReplaceAll(base64Sign, "/", "_")
	base64Sign = strings.TrimRight(base64Sign, "=")
	// Сверяем и возвращаем
	return base64Sign == rawParams["sign"][0], nil
}
