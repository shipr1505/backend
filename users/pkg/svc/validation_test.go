package svc

import (
	"fmt"
	mio "gitlab.com/shipr/backend/matchmaking/pkg/io"
	"gitlab.com/shipr/backend/matchmaking/pkg/pb"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"testing"

	"github.com/SevereCloud/vksdk/api"
)

func TestValidation(t *testing.T) {
	status, err := isValid(
		"https://localhost:10888/?vk_access_token_settings=friends%2Cstatus&vk_app_id=7289725&vk_are_notifications_enabled=0&vk_is_app_user=1&vk_is_favorite=0&vk_language=ru&vk_platform=desktop_web&vk_ref=other&vk_user_id=201053498&sign=1TvZGO9hbCQZukAdIDXirn2Yl8rv-y0DvttjSYXpZ2U",
		"ULPZAvI57zxLU5eGc18m",
	)
	if err != nil {
		panic(err)
	}
	fmt.Println("status: ", status)
}

func TestVKAPI(t *testing.T) {
	vk := api.NewVK("72c1cae172c1cae172c1cae1da72aef19c772c172c1cae12ce734dff230269fc3eb668e")
	response, err := vk.FriendsGetFields(
		api.Params{
			// id184918216
			"user_id": "174050430",
			"fields":  "sex,bdate",
		},
	)

	if err != nil {
		t.Error(err)
		return
	}

	fmt.Println(response.Items[0].Bdate)
}

func TessstUpdateFriends(t *testing.T) {
	sess, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}

	c := sess.DB("shipper").C("users")

	friends := []*pb.User{
		{Id: "cunt", Gender: true},
	}
	var friendUpserts []interface{}
	friendsIDs := make([]string, 0, len(friends))
	for _, friend := range friends {
		friendsIDs = append(friendsIDs, friend.Id)

		/*
			bd, err := ptypes.Timestamp(friend.BirthDate)
			if err != nil {
				panic(err)
			}
		*/

		friendUser := mio.User{
			ID:     mio.UID(friend.Id),
			Gender: friend.Gender,
			//BirthDate:    bd,
			IsRegistered: false,
		}
		friendUser.SetDefaults()

		friendUpserts = append(
			friendUpserts,
			[]interface{}{
				bson.M{"vk_id": friend.Id},
				bson.M{"$setOnInsert": friendUser},
			}...,
		)
	}

	fmt.Println("friend upserts", len(friendUpserts))

	bulk := c.Bulk()
	bulk.Unordered()
	bulk.Upsert(friendUpserts...)
	bulk.Upsert(
		bson.M{
			"vk_id": "denis",
		},
		bson.M{
			"$set": bson.M{"friends": friendsIDs},
		},
	)

	if _, err := bulk.Run(); err != nil {
		panic(err)
	}
}
