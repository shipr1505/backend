package svc

import (
	"gitlab.com/shipr/backend/misc"
	"time"

	mio "gitlab.com/shipr/backend/matchmaking/pkg/io"

	"github.com/dgrijalva/jwt-go"
)

// generateToken creates a new jwt token
func generateToken(signingKey []byte, clientID mio.UID) (string, error) {
	claims := misc.JWTClaims{
		ID: clientID,
		StandardClaims: jwt.StandardClaims{
			// jwt expires in a month
			ExpiresAt: time.Now().Add(time.Hour * 24 * 30).Unix(),
			IssuedAt:  jwt.TimeFunc().Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(signingKey)
}
