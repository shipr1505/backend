package io

import (
	"github.com/golang/protobuf/ptypes"
	mio "gitlab.com/shipr/backend/matchmaking/pkg/io"
	"gitlab.com/shipr/backend/users/pkg/pb"
)

func MakePbProfile(user *mio.User) (*pb.Profile, error) {
	pbBirthDate, err := ptypes.TimestampProto(user.BirthDate)
	if err != nil {
		return nil, err
	}
	pbPreferredAge := pb.Profile_AgeInterval{
		MinAge: ptypes.DurationProto(user.PreferredAge.MinAge),
		MaxAge: ptypes.DurationProto(user.PreferredAge.MaxAge),
	}
	return &pb.Profile{
		BirthDate: pbBirthDate,
		PreferredAge: &pbPreferredAge,
		PreferredGender: user.PreferredGender,
	}, nil
}
