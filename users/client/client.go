package client

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/micro/go-micro/v2/client"
	"gitlab.com/shipr/backend/misc"
	"gitlab.com/shipr/backend/users/pkg/pb"
)

type usersHTTPHandler struct {
	cl pb.UsersService
}

// SamplePair wraps sample pair
func (h *usersHTTPHandler) Auth(w http.ResponseWriter, r *http.Request) {
	var request pb.AuthRequest
	if err := misc.ReadJSON(&request, r); err != nil {
		misc.WriteError(err, w)
		return
	}

	ctx := misc.HeaderToContext(r.Header)
	response, err := h.cl.Auth(ctx, &request)
	if err != nil {
		misc.WriteError(err, w)
		return
	}

	misc.WriteJSON(response, w)
}

// SamplePair wraps sample pair
func (h *usersHTTPHandler) Refresh(w http.ResponseWriter, r *http.Request) {
	var request pb.RefreshRequest
	if err := misc.ReadJSON(&request, r); err != nil {
		misc.WriteError(err, w)
		return
	}

	ctx := misc.HeaderToContext(r.Header)
	response, err := h.cl.Refresh(ctx, &request)
	if err != nil {
		misc.WriteError(err, w)
		return
	}

	misc.WriteJSON(response, w)
}

// UpdateProfile wraps update profile
func (h *usersHTTPHandler) UpdateProfile(w http.ResponseWriter, r *http.Request) {
	var request pb.UpdateProfileRequest
	if err := misc.ReadJSON(&request, r); err != nil {
		misc.WriteError(err, w)
		return
	}

	ctx := misc.HeaderToContext(r.Header)
	response, err := h.cl.UpdateProfile(ctx, &request)
	if err != nil {
		misc.WriteError(err, w)
		return
	}

	misc.WriteJSON(response, w)
}

// UpdateProfile wraps update profile
func (h *usersHTTPHandler) GetProfile(w http.ResponseWriter, r *http.Request) {
	var request pb.GetProfileRequest
	if err := misc.ReadJSON(&request, r); err != nil {
		misc.WriteError(err, w)
		return
	}

	ctx := misc.HeaderToContext(r.Header)
	response, err := h.cl.GetProfile(ctx, &request)
	if err != nil {
		misc.WriteError(err, w)
		return
	}

	misc.WriteJSON(response, w)
}

// UpdateProfile wraps update profile
func (h *usersHTTPHandler) UpdateFriends(w http.ResponseWriter, r *http.Request) {
	var request pb.UpdateFriendsRequest
	if err := misc.ReadJSON(&request, r); err != nil {
		misc.WriteError(err, w)
		return
	}

	ctx := misc.HeaderToContext(r.Header)
	response, err := h.cl.UpdateFriends(ctx, &request)
	if err != nil {
		misc.WriteError(err, w)
		return
	}

	misc.WriteJSON(response, w)
}

// New creates a new matchmaking client
func New(client client.Client) http.Handler {
	uh := usersHTTPHandler{
		cl: pb.NewUsersService("go.micro.srv.users", client),
	}

	r := mux.NewRouter()

	r.HandleFunc("/auth", uh.Auth)
	r.HandleFunc("/refresh", uh.Refresh)
	r.HandleFunc("/update_profile", uh.UpdateProfile)
	r.HandleFunc("/get_profile", uh.GetProfile)
	r.HandleFunc("/update_friends", uh.UpdateFriends)

	return r
}
