FROM golang

# go watcher doesn't work outside gopath
RUN mkdir -p /go/src/gitlab.com/shipr/backend
WORKDIR /go/src/gitlab.com/shipr/backend

COPY . ./

RUN go mod download
RUN go install github.com/canthefason/go-watcher/cmd/watcher

ENTRYPOINT watcher -run gitlab.com/shipr/backend/users/cmd -watch gitlab.com/shipr/backend/users