package main

import (
	"github.com/micro/cli/v2"
	"log"
	"os"

	"github.com/micro/go-micro/v2"
	_ "github.com/micro/go-plugins/registry/consul/v2"
	"gitlab.com/shipr/backend/misc"
	"gitlab.com/shipr/backend/users/pkg/pb"
	"gitlab.com/shipr/backend/users/pkg/svc"
	"gopkg.in/mgo.v2"
)

var (
	consulAddr string
	mongoAddr  string

	vkSecret string
	vkServiceToken string
)

func main() {
	service := micro.NewService(
		micro.Name("go.micro.srv.users"),
		micro.WrapHandler(misc.RecoveryWrapper),

		micro.Address(":5000"),

		micro.Flags(
			&cli.StringFlag{
				Name:  "consul.addr",
				Value: "consul:8500",
				Usage: "Consul agent address",
			},
			&cli.StringFlag{
				Name:  "mongo.addr",
				Value: "mongo:27017",
				Usage: "MongoDB address",
			},
			&cli.StringFlag{
				Name:  "vk.secret",
				Value: "ULPZAvI57zxLU5eGc18m",
				Usage: "VK Secure Key",
			},
			&cli.StringFlag{
				Name:"vk.service.token",
				Value: "72c1cae172c1cae172c1cae1da72aef19c772c172c1cae12ce734dff230269fc3eb668e",
				Usage: "VK Service Token",
			},
		),
	)

	service.Init(
		micro.Action(func(c *cli.Context) error {
			consulAddr = c.String("consul.addr")
			mongoAddr = c.String("mongo.addr")
			vkSecret = c.String("vk.secret")
			vkServiceToken = c.String("vk.service.token")
			return nil
		}),
	)

	os.Setenv("MICRO_REGISTRY_ADDRESS", consulAddr)
	os.Setenv("MICRO_REGISTRY", "consul")

	var handler *svc.UsersHandler
	{
		msess, err := mgo.Dial(mongoAddr)
		if err != nil {
			log.Fatal(err)
		}

		handler = svc.NewUsersHandler(vkSecret, vkServiceToken, msess)
	}

	pb.RegisterUsersHandler(service.Server(), handler)

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
